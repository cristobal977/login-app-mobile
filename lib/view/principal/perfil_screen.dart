import 'package:flutter/material.dart';

class PerfilScreen extends StatelessWidget {
  const PerfilScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple[900],
        title: Text("Perfil"),
        elevation: 0,
      ),
      body: Center(
        child: Text("PerfilScreen"),
      ),
    );
  }
}
