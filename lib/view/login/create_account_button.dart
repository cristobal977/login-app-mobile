import 'package:flutter/material.dart';
import 'package:tarea_login/repository/user_repository.dart';
import 'package:tarea_login/view/register/register_screen.dart';

class CreateAccountButton extends StatelessWidget {
  final UserRepository _userRepository;

  CreateAccountButton({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerRight,
          end: Alignment.centerLeft,
          colors: [
            Colors.deepPurple[900],
            Colors.deepPurple[600],
          ],
        ),
      ),
      child: MaterialButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return RegisterScreen(
                  userRepository: _userRepository,
                );
              },
            ),
          );
        },
        child: Center(
          child: Text(
            "Registrarse",
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
      ),
    );
  }
}
