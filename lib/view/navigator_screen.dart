import 'package:flutter/material.dart';
import 'package:tarea_login/view/principal/home_screen.dart';
import 'package:tarea_login/view/principal/perfil_screen.dart';
import 'package:tarea_login/view/principal/search_screen.dart';

class NavigatorScreen extends StatefulWidget {
  final String name;

  NavigatorScreen({Key key, @required this.name}) : super(key: key);

  @override
  _NavigatorScreenState createState() => _NavigatorScreenState();
}

class _NavigatorScreenState extends State<NavigatorScreen> {
  int _index = 0;
  @override
  Widget build(BuildContext context) {
    Widget child;
    switch (_index) {
      case 0:
        child = HomeScreen(name: widget.name);
        break;
      case 1:
        child = SearchScreen();
        break;
      case 2:
        child = PerfilScreen();
        break;
    }
    return Scaffold(
      body: SizedBox.expand(child: child),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(8),
            topLeft: Radius.circular(8),
          ),
          boxShadow: [
            BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 4),
          ],
        ),
        child: ClipRRect(
          child: BottomNavigationBar(
            backgroundColor: Colors.deepPurple[900],
            selectedItemColor: Colors.white,
            unselectedItemColor: Colors.white70,
            currentIndex: _index,
            onTap: (i) => setState(() => _index = i),
            showUnselectedLabels: true,
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.explore),
                label: "Explorar",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.search),
                label: "Buscar",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: "Perfil",
              )
            ],
          ),
        ),
      ),
    );
  }
}
