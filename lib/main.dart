import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tarea_login/bloc/authentication_bloc/bloc.dart';
import 'package:tarea_login/bloc/simple_bloc_observer.dart';
import 'package:tarea_login/repository/user_repository.dart';
import 'package:tarea_login/view/navigator_screen.dart';
import 'package:tarea_login/view/login/login_screen.dart';
import 'package:tarea_login/view/splash_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  //BlocSupervisor.delegate = SimpleBlocDelegate();
  Bloc.observer = SimpleBlocObserver();

  final UserRepository userRepository = UserRepository();
  runApp(BlocProvider(
    create: (context) =>
        AuthenticationBloc(userRepository: userRepository)..add(AppStarted()),
    child: App(userRepository: userRepository),
  ));
}

class App extends StatelessWidget {
  final UserRepository _userRepository;

  App({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is Uninitialized) {
            return SplashScreen();
          }
          if (state is Authenticated) {
            return NavigatorScreen(
              name: state.displayName,
            );
          }
          if (state is Unauthenticated) {
            return LoginScreen(
              userRepository: _userRepository,
            );
          }
          return Container();
        },
      ),
    );
  }
}
